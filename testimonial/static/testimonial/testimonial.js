$(document).ready(function(){
    showTestimonial()

    $("#testi-sub").click(function(e){
        submitForm();
    })
})


function showTestimonial() {
    var dataRes = ""

    $.ajax({
        url : url,
        error: function(){
            alert("Something went wrong, try again.")
        },
        success: function(data){
            console.log(data);
            
            $('#testimonial-result').empty();
            if(typeof (data.data) === 'undefined'){
                alert("No book found, try another keyword");
            }

            for(var counter=0; counter<data.data.length; counter++){
                dataRes += '<div class="card b-green mt-2r w-100">';
                dataRes += '<h2> ' +  data.data[counter].name + ' </h2>';
                var date = convertTime(data.data[counter].time);
                dataRes += '<p class="small-font" style="margin-top:-0.5rem;"> ' +  date + ' </p>';
                dataRes += '<p style="margin-top:-0.5rem;"> ' +  data.data[counter].message + ' </p>';
                dataRes += '</div>';
            }
            console.log(dataRes);
            $("#testimonial-result").append(dataRes);
        },

    })
}

function convertTime(process) {
    var dayArr = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
    var monthArr = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    var timeObj = new Date(process);
    var year = timeObj.getFullYear();
    var month = monthArr[timeObj.getMonth()];
    var date = timeObj.getDate();
    var day = dayArr[timeObj.getDay()];
    var hour = timeObj.getHours();
    if(hour < 10){
        hour = "0" + hour;
    }
    var minute = timeObj.getMinutes();
    if(minute < 10){
        minute = "0" + minute;
    }

    console.log(typeof(minute));
    return day + ", " + date + " " + month + " " + year + " | " + hour + ":" + minute;
}


function submitForm(){
    $.ajax({
        type: "POST",
        url : url2,
        data: $("form#form-testi").serialize(),
        error: function(){
            alert("Something went wrong, try again.")
        },
        success: function(data){
            console.log(data)
            if (data.success === true) {
                showTestimonial();
                window.setTimeout(function() {
                    alert("Success!");
                }, 200);
            } else if (data.success === false) {
                alert("Failed")
            }
        },
        
    })
}