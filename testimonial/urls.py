from django.urls import path
from . import views

app_name = 'testimonial'

urlpatterns = [
    path('', views.testimonial, name='testimonial'),
    path('json/', views.get_JSON_testimonial, name='testimonial_JSON')
]
