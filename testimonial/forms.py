from django import forms

class TestimoniForm(forms.Form):
    message = forms.CharField(label="Testimoni", widget=forms.Textarea(attrs={
        'rows': 6,
        'style': 'resize:none;',
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Testimoninya :)',
        'required': True,
    }))