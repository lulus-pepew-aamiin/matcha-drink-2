from django.db import models

# Create your models here.
class Testimonial(models.Model):
    name = models.CharField(max_length=50, blank=False)
    message = models.CharField(max_length=300, blank=False)
    time = models.DateTimeField(auto_now_add=True, blank=False)

    def get_dict(self):
        return {
            'name':self.name,
            'message':self.message,
            'time':self.time
        }