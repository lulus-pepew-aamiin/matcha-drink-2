from django.contrib.auth.models import User
from django.test import TestCase
from .forms import TestimoniForm
from .models import Testimonial
import json

# Create your tests here.
class TestimonialHTMLTest(TestCase):
    def setUp(self):
        User.objects.create_user("nanda", None, "nanda")

    def test_text(self):
        response = self.client.get('/testimonial/')

        self.assertIn('Masuk dulu ya', response.content.decode())

        self.client.login(username="nanda", password="nanda")
        response = self.client.get('/testimonial/')
        self.assertIn('Testimoni:', response.content.decode())

    def test_submit_button(self):
        response = self.client.get('/testimonial/')
        self.assertNotIn('Submit', response.content.decode())

        self.client.login(username="nanda", password="nanda")
        response = self.client.get('/testimonial/')
        self.assertIn('Submit', response.content.decode())

    def test_form(self):
        self.client.login(username="nanda", password="nanda")
        response = self.client.get('/testimonial/')
        self.assertIn('<form', response.content.decode())
        self.assertIn('<input', response.content.decode())

class TestimonialViewsTest(TestCase):
    def setUp(self):
        User.objects.create_user("nanda", None, "nanda")
    
    def test_form_valid(self):
        self.client.login(username="nanda", password="nanda")

        headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        response = self.client.post('/testimonial/', **headers, data={
            'message': 'hello',
        })
        self.assertTrue(Testimonial.objects.filter(message="hello").exists())

    def test_form_invalid(self):
        headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        response = self.client.post('/testimonial/', **headers, data={
            'message': 'hello',
        })
        self.assertFalse(Testimonial.objects.filter(message="hello").exists())

class TestimonialModelTest(TestCase):
    def setUp(self):
        data = Testimonial(
            name="Nanda",
            message="Hello World!",
        )
        data.save()

    def test_get_JSON_correct(self):
        response = self.client.get("/testimonial/json/")
        testimoni = Testimonial.objects.all()[::-1][:5]
        self.assertEqual(len(json.loads(response.content)['data']), len(testimoni))