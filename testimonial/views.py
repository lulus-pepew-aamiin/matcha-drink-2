from django.contrib.auth.models import User
from django.shortcuts import render, HttpResponse, redirect
from django.http import JsonResponse
from .models import Testimonial
from .forms import TestimoniForm
import json

# Create your views here.
def testimonial(request):
    responses = {}
    if request.method == 'POST':
        if request.is_ajax():
            if request.user.is_authenticated:
                form = TestimoniForm(request.POST)
                if form.is_valid():
                    name = request.user.username
                    message = form.cleaned_data['message']
                    Testimonial(name=name, message=message).save()
                    return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    else:
        form = TestimoniForm()
        responses['form'] = form
        return render(request, 'testimonial/testimonial.html', responses)

def get_JSON_testimonial(request):
    data = {
        'data':[i.get_dict() for i in Testimonial.objects.all()[::-1][:5]],
    }
    return JsonResponse(data=data)