MatchA Drink v2
============

[![Pipeline](https://gitlab.com/lulus-pepew-aamiin/matcha-drink-2/badges/master/pipeline.svg)](https://gitlab.com/lulus-pepew-aamiin/matcha-drink-2/pipelines)
[![Coverage](https://gitlab.com/lulus-pepew-aamiin/matcha-drink-2/badges/master/coverage.svg)](https://gitlab.com/lulus-pepew-aamiin/matcha-drink-2/pipelines)

Anggota Kelompok __KA04__:
* Aji Inisti Udma Wijaya (1806141126)
* M. Oktoluqman Fakhrianto (1806186723)
* Nanda Ryaas A. (1806141391)
* Taufik Algi Fahri (1806205136)

Link Heroku: [MatchA Drink v2](https://matcha-drink-2.herokuapp.com)

## Penjelasan

Aplikasi ini bertujuan untuk mempermudah pengguna dalam memesan minuman secara online.
Dengan aplikasi ini, minuman yang dipesan orang akan langsung dimasukkan ke antrian untuk segera dibuat.

## Fitur-fitur

* Beranda. Daftar menu favorit (Algi)
* Halaman Pilihan. Pilih menu dan beli (Okto)
* Halaman Tentang. Tentang MatchA Drink (Aji)
* Halaman Kontak. Kontak dan testimoni (Ryaas)
