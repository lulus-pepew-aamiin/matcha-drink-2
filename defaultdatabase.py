from menu.models import Menu
from web.models import FavoriteMenu

menu1 = Menu(
    name='Matchiato',
    price=18000,
    description='Sentuhan foam diatas espresso dengan gaya Eropa yang kekinian.',
    favorite=True,
    image='menu/matchiato.png',
)
menu1.save()
menu2 = Menu(
    name='Matchinno',
    price=15000,
    description=(
        'Espresso dengan susu dan lapisan foam yang dalam. '
        'Minuman eksklusif MatchA Drink yang dibuat hanya untukmu.'
    ),
    favorite=True,
    image='menu/matchinno.png',
)
menu2.save()
Menu(
    name='Caffè Latte',
    price=18000,
    description=(
        'Espresso dengan susu hangat dan foam. Caffè latte memiliki '
        'rasa sesegar air gunung.'
    ),
    image='menu/caffe_latte.png',
).save()
Menu(
    name='Americano',
    price=18000,
    description='Kopi espresso dengan tambahan air hangat',
    image='menu/americano.png',
).save()
Menu(
    name='Espresso',
    price=18000,
    description=(
        'Selembut sutra dan memiliki bau yang menggugah selera. '
        'Espresso adalah minuman yang cocok untuk bersantai.'
    ),
    image='menu/espresso.png',
).save()
Menu(
    name='Asian Dolce Latte',
    price=18000,
    description='Double-shots yang dikombinasikan dengan saus dolce',
    image='menu/asian_doice_latte.png',
).save()
Menu(
    name='Chocolate Cream Frappucino',
    price=18000,
    description=(
        'Dipenuhi dengan rasa moka, choco chips (bukan donat), susu dan es '
        'membuat rasa yang menakjubkan.'
    ),
    image='menu/choco_cream_frap.png',
).save()

FavoriteMenu(menu=menu1, image='web/matchiato.png').save()
FavoriteMenu(menu=menu2, image='web/matchinno.png').save()
