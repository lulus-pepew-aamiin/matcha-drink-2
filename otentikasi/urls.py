from django.urls import path
from . import views

app_name = 'otentikasi'

urlpatterns = [
    path('login', views.ajax_login, name='login'),
    path('register', views.ajax_register, name='register'),
    path('logout', views.ajax_logout, name='logout'),
]
