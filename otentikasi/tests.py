from django.test import TestCase


class OtentikasiTest(TestCase):
    def test_login_page(self):
        headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        response = self.client.get('/otentikasi/login', **headers)

        self.assertContains(response, 'Username')
        self.assertContains(response, 'Password')
        self.assertContains(response, '<input', count=3)

    def test_register_page(self):
        headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        response = self.client.get('/otentikasi/register', **headers)

        self.assertContains(response, 'Email')
        self.assertContains(response, 'Username')
        self.assertContains(response, 'Password')
        self.assertContains(response, 'Konfirmasi Password')
        self.assertContains(response, '<input', count=5)

    def test_logout(self):
        pass
