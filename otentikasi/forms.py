from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, UsernameField
from django.contrib.auth.models import User


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email')
        field_classes = {'username': UsernameField}
        labels = {
            'username': 'Username',
            'email': 'Email',
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = 'Ryaasssssssss gantenk'
        self.fields['email'].widget.attrs['placeholder'] = 'kepo deh kamu'
        self.fields['password1'].label = 'Password'
        self.fields['password1'].widget.attrs['placeholder'] = 'kepo deh kamu'
        self.fields['password2'].label = 'Konfirmasi Password'
        self.fields['password2'].widget.attrs['placeholder'] = 'kepo deh kamu'

class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['username'].label = 'Username'
        self.fields['username'].widget.attrs['placeholder'] = 'Ryaasssssssss gantenk'
        self.fields['password'].label = 'Password'
        self.fields['password'].widget.attrs['placeholder'] = 'kepo deh kamu'
