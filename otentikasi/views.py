from django.contrib.auth import login, logout
from django.http import JsonResponse
from django.shortcuts import Http404, render

from .forms import LoginForm, RegisterForm


def ajax_login(request):
    if request.is_ajax():
        if request.method == 'POST':
            form = LoginForm(request, request.POST)
            if form.is_valid():
                user = form.get_user()
                login(request, user)
                return JsonResponse({'html': '', 'success': True, 'username': user.username})
        else:
            form = LoginForm(request)

        html = render(request, 'otentikasi/login.html', {'form': form}).content.decode()
        return JsonResponse({'html': html})

    raise Http404('Halaman tidak ditemukan.')

def ajax_register(request):
    if request.is_ajax():
        if request.method == 'POST':
            form = RegisterForm(request.POST)
            if form.is_valid():
                user = form.save()
                login(request, user)
                return JsonResponse({'html': '', 'success': True, 'username': user.username})
        else:
            form = RegisterForm()

        html = render(request, 'otentikasi/login.html', {'form': form}).content.decode()
        return JsonResponse({'html': html})

    raise Http404('Halaman tidak ditemukan.')

def ajax_logout(request):
    if request.user.is_authenticated:
        logout(request)
        return JsonResponse({'html': '', 'success': True})
    return JsonResponse({'html': '', 'success': False})
