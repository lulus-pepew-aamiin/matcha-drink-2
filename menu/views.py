from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import Http404, get_object_or_404, redirect, render
from django.urls import reverse

from .forms import OrderForm
from .models import Cart, Menu, Order


def menu(request):
    if request.method == 'POST' and request.user.is_authenticated:
        form = OrderForm(request.POST)
        if form.is_valid():
            user = request.user
            if user is not None:
                cart = Cart(user=user)
            else:
                cart = Cart(user='user')

            cart.save()
            for key, value in form.cleaned_data.items():
                if key[:5] == 'menu_' and int(value) > 0:
                    order = Order(
                        cart=cart,
                        menu=get_object_or_404(Menu, id=key[5:]),
                        amount=value,
                    )
                    order.save()

            return redirect(reverse('menu:cart', kwargs={'cart_id': cart.id}))

    else:
        form = OrderForm()

    favorite_menu = Menu.objects.filter(favorite=True)
    for each in favorite_menu:
        each.form_field = form['menu_' + str(each.id)]

    other_menu = Menu.objects.filter(favorite=False)
    for each in other_menu:
        each.form_field = form['menu_' + str(each.id)]

    context = {
        'favorite_menu': favorite_menu,
        'other_menu': other_menu,
        'form': form,
    }

    return render(request, 'menu/menu.html', context)


def cart_detail(request, cart_id):
    cart = get_object_or_404(Cart, id=cart_id)
    all_orders = cart.order_set.all()

    subtotal = 0
    for order in all_orders:
        subtotal += order.amount * order.menu.price

    context = {
        'orders': cart.order_set.all(),
        'subtotal': subtotal,
    }

    return render(request, 'menu/cart.html', context)

def search_menu(request):
    if request.is_ajax():
        filtered = Menu.objects.filter(name__icontains=request.GET.get('q', ''))
        data = serializers.serialize('json', filtered)
        return HttpResponse(data, content_type='application/json')
    raise Http404()
