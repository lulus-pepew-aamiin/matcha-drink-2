from django.shortcuts import render, redirect

from .models import FavoriteMenu, UserVisit
from otentikasi.forms import RegisterForm, LoginForm
from .forms import User

# Create your views here.
def index(request):

    if request.method == 'POST':
        form_user = User(request.POST)
        if form_user.is_valid():
            form = UserVisit()
            user = form_user.cleaned_data['user_name']
            form.save()

            request.session['user_name'] = user

    favorite_menu_home = FavoriteMenu.objects.filter(menu__favorite=True)
    form_user = User()
    form_login = LoginForm();
    form_register = RegisterForm();
    context = {
        'favorite_menu_home' : favorite_menu_home,
        'form_user' : form_user,
        'form_login' : form_login,
        'form_register' : form_register,
    }
    return render(request, 'web/index.html', context)
