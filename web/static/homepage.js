$(document).ready(function () {

    $('a[data-toggle=modal]').click(function() {
        $.ajax({
            url: "/otentikasi/login",
            type: "GET",
            success: function(result) {
                console.log(result)
                $('div#form_login').html(result.html)
            }
        })
    });

    $('button[name=button-modal-daftar]').click(function() {
        console.log("test");
        // alert('clicked')
        $('button[name=button-modal-masuk]').removeClass("clicked").addClass("not-clicked");
        $('#form-regis').removeClass("hidden");
        $('button[name=button-modal-daftar]').removeClass("not-clicked").addClass("clicked");
        $('#form-login').addClass("hidden");

        $.ajax({
            url: "/otentikasi/register",
            type: "GET",
            success: function(result) {
                console.log(result)
                $('div#form_register').html(result.html)

            }
        })

    });

    $('button[name=button-daftar]').click(function() {
        $.ajax({
            url: "/otentikasi/register",
            type: "POST",
            data: $('form#form-regis').serialize(),
            success: function(result) {
                console.log(result)
                $('div#form_register').html(result.html)
                if (result.html === "") {
                    // $('body').html(result.username)
                    window.location.reload();
                }
            }
        })
    })

    $('button[name=button-modal-masuk]').click(function() {
        console.log("uhuyy");
        $('button[name=button-modal-masuk]').removeClass("not-clicked").addClass("clicked");
        $('#form-login').removeClass("hidden");
        $('button[name=button-modal-daftar]').removeClass("clicked").addClass("not-clicked");
        $('#form-regis').addClass("hidden");

        $.ajax({
            url: "/otentikasi/login",
            type: "GET",
            success: function(result) {
                console.log(result)
                $('div#form_login').html(result.html)
            }
        })
    });

    $('button[name=button-masuk]').click(function() {
        $.ajax({
            url: "/otentikasi/login",
            type: "POST",
            data: $('form#form-login').serialize(),
            success: function(result) {
                console.log(result)
                $('div#form_login').html(result.html)
                if (result.html === "") {
                    window.location.reload();
                }
            }
        })
    })

    $("a#logout").click(function() {
        $.ajax({
            url: "/otentikasi/logout",
            type: "GET",
            data: $('a#logout').serialize(),
            success: function(result) {
                console.log(result)
                if (result.html === "") {
                    window.location.reload();
                }
            }
        })
    });
});
