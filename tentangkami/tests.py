from django.test import TestCase, Client
from django.urls import resolve
from .models import EmailModel
from .forms import EmailForms
from .views import masukan_data, get_data
from django.contrib.auth.models import User

# Create your tests here.
class AboutUsTest(TestCase):

    def setUp(self):
        super().setUp()
        User.objects.create_user('lalalalalala', None, 'inisti')

    def test_about_us_url_valid(self):
        response = Client().get('/tentang_kami/')
        self.assertEqual(response.status_code, 200)

    def test_about_us_template(self):
        response = self.client.get('/tentang_kami/')
        self.assertTemplateUsed(response, 'index.html')

    def test_email_form(self):
        response = self.client.get('/tentang_kami/')
        self.assertContains(response, '<button')

    def test_view(self):
        email = EmailModel.objects.create(email='wijaya@gmail.com')
        data = {'email' : email.email}
        form = EmailForms(data=data)
        self.assertTrue(form.is_valid())

    def test_view_post(self):
        data = {'email' : 'ajiinisti@gmail.com'}
        response = Client().post('/tentang_kami/', data)
        self.assertEqual(response.status_code, 200)

    def test_database(self):
        email = EmailModel(email='ajiinisti@gmail.com')
        email.save()
        hitungjumlah = EmailModel.objects.all().count()
        self.assertEqual(hitungjumlah, 1)

    def test_url_ajax_post(self):
        data = {'email' : 'ajiinisti@gmail.com'}
        response = self.client.post("/tentang_kami/masukan_data/",data)
        self.assertEqual(response.status_code,200)

    def test_ajax_post_views(self):
        response = resolve("/tentang_kami/masukan_data/")
        self.assertEqual(response.func,masukan_data)

    def test_url_ajax_get(self):
        response = self.client.get("/tentang_kami/get_data/")
        self.assertEqual(response.status_code,200)

    def test_ajax_get_views(self):
        response = resolve("/tentang_kami/masukan_data/")
        self.assertEqual(response.func,masukan_data)

    def test_login_authenticated(self):
        self.client.login(username='lalalalalala', password='inisti')
        response = self.client.get('/tentang_kami/')
        self.assertContains(response, 'lalalalalala')
