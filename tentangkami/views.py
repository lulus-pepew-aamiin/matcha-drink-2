from django.shortcuts import render,redirect
from django.core import serializers
from django.http import HttpResponse,JsonResponse
from .forms import EmailForms
from .models import EmailModel
from django.contrib.auth.models import User

# Create your views here.
def tentang_kami(request):   
    form = EmailForms(request.POST or None)
    return render(request,"index.html", {'generated_form': form})

def masukan_data(request):
    email = request.POST['email']
    model = EmailModel(email = email)
    model.save()
    response_data = {}
    response_data['email'] = email
    response_data['banyakData'] = EmailModel.objects.all().count() + User.objects.all().count()
    return JsonResponse(response_data)

def get_data(request):
    email_list = list(EmailModel.objects.values('email'))
    banyakData = len(email_list)
    return JsonResponse({'banyakEmail':banyakData , 'email':email_list})

