from django.urls import path
from . import views

app_name = 'tentangkami'

urlpatterns = [
    path('', views.tentang_kami, name='tentangkami'),
    path('masukan_data/',views.masukan_data),
    path('get_data/',views.get_data),
]
